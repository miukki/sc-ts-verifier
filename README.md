
## Available Scripts
```
#truffle
truffle compile
truffle migrate


#react
yarn --cwd client start
```

# Test
```
truffle test
```

# Connection to metamask
```
  create gaanchec network 
  host: '127.0.0.1',
  port: 7545,
```

# Up local blockchain
```
#for ganache
  host: '127.0.0.1',
  port: 7545,


```