const path = require('path');

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  contracts_build_directory: path.join(__dirname, 'client/src/contracts'),
  networks: {
    develop: {
      host: '127.0.0.1',
      port: 7545,
      network_id: '5777', //1337
      gas: 6721975, // Gas limit used for deploys
    },
  },
  compilers: {
    solc: {
      version: '0.8.4',
    },
  },
};
