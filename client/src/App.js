import React, { Component } from 'react';
import VerifierContract from './contracts/Verifier.json';
import getWeb3 from './getWeb3';

import './App.css';

class App extends Component {
  state = { response: 0, web3: null, accounts: null, contract: null };

  componentDidMount = async () => {
    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = VerifierContract.networks[networkId];
      const instance = new web3.eth.Contract(
        VerifierContract.abi,
        deployedNetwork && deployedNetwork.address
      );

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.
      this.setState({ web3, accounts, contract: instance }, this.runExample);
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`
      );
      console.error(error);
    }
  };

  runExample = async () => {
    const { accounts, contract } = this.state;

    try {
      const response = await contract.methods
        .verifyProof(
          [
            `2931310250919339805514457396232938091269926224594068286655355694594150489124`,
            `12860443821078296158920358965360651859522305263915687365674945795497035457994`,
          ],
          [
            [
              `9901418398054855021484222005390834652381678997749598676254058996035609671887`,
              `14225586145823670512811009726826990842241586584806300569333038892982172676459`,
            ],
            [
              `9286160792941796683935040555941676968860743092027557818094558453651005381838`,
              `8717585140862240974093071139308659044943113415110296753827109841314996701458`,
            ],
          ],
          [
            `17773737980773350212266162288570917287010758985722974489308417975289876511221`,
            `10450509548482029645323943232270875068435328587621517190939076226882701996372`,
          ],
          [
            `20667348647680622753734948506453029608295328224149426724659072774346554447381`,
          ]
        )
        .call();

      // Get the value from the contract to prove it worked.

      console.log(`response`, response);
      // Update state with the result.
      this.setState({ response });
    } catch (err) {
      console.log(`err`, err);
      this.setState({ response: `error` });
    }
  };

  render() {
    if (!this.state.web3) {
      return <div>Loading Web3, accounts, and contract...</div>;
    }
    return (
      <div className="App">
        <div>Verified: {JSON.stringify(this.state.response)}</div>
      </div>
    );
  }
}

export default App;
